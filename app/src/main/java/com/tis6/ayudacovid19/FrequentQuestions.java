package com.tis6.ayudacovid19;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class FrequentQuestions extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frequent_questions);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbarquestionsfrecuent);

    }
    public void onClick(View view) {
        Intent myIntent;
        myIntent = null;
        switch (view.getId()){
            case R.id.btnhomeLauncher:

                myIntent=new Intent(FrequentQuestions.this,PatientView.class);
                break;


            case R.id.btnuserLauncher:
                myIntent=new Intent(FrequentQuestions.this,UserProfile.class);
                break;

        }
        startActivity(myIntent);


    }

}