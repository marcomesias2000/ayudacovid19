package com.tis6.ayudacovid19;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;


public class RecyclerViewAdaptador extends RecyclerView.Adapter<RecyclerViewAdaptador.ViewHolder>
{
    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView namepatient, surnamepatient, dnipatient, ubicationpatient;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            namepatient=(TextView)itemView.findViewById(R.id.txtNamePatient);
            surnamepatient=(TextView)itemView.findViewById(R.id.txtSurnamePatient);
            dnipatient=(TextView)itemView.findViewById(R.id.txtdnipatient);
            ubicationpatient=(TextView)itemView.findViewById(R.id.txtubicationpatient);
        }
    }
    public List<Patient> pacienteMedicoList;
    public RecyclerViewAdaptador(List<Patient> pacienteMedicoList)
    {
        this.pacienteMedicoList = pacienteMedicoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.listmanagementpatient,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.namepatient.setText(pacienteMedicoList.get(position).getNamepatient());
        holder.surnamepatient.setText(pacienteMedicoList.get(position).getSurnamepatient());
        holder.dnipatient.setText(pacienteMedicoList.get(position).getDnipatient());
        holder.ubicationpatient.setText(pacienteMedicoList.get(position).getUbicationpatient());
    }

    @Override
    public int getItemCount() {
        return pacienteMedicoList.size();
    }
}
