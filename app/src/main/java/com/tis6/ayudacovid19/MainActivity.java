package com.tis6.ayudacovid19;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MainActivity extends AppCompatActivity

{
    // Declaring layout button, edit texts
    Button login;
    EditText username,password;
    ProgressBar progressBar;
    // End Declaring layout button, edit texts

    // Declaring connection variables
    Connection con;
    String un,pass,db,ip,usuario,contraseña;
    //End Declaring connection variables

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbarlogin);

        // Getting values from button, texts and progress bar
        login = (Button) findViewById(R.id.btnLogIn);
        username = (EditText) findViewById(R.id.textUser);
        password = (EditText) findViewById(R.id.textPassword);
        progressBar = (ProgressBar) findViewById(R.id.pbarLogIn);
        // End Getting values from button, texts and progress bar

        // Declaring Server ip, username, database name and password
        ip = "sqlserverdb.chjctcdkykee.sa-east-1.rds.amazonaws.com";
        db = "control_paciente_domicilio_covid_19";
        un = "admin";
        pass = "sqlserver";
        // Declaring Server ip, username, database name and password

        // Setting up the function when button login is clicked
        login.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CheckLogin checkLogin = new CheckLogin();// this is the Asynctask, which is used to process in background to reduce load on app process
                checkLogin.execute("");
            }
        });
        //End Setting up the function when button login is clicked
    }
    public void onClick(View view)
    {
        Intent myIntent = new Intent(MainActivity.this, ForgotPasswordActivity.class);
        startActivity(myIntent);
    }

    public class CheckLogin extends AsyncTask<String,String,String>
    {
        String z = "";
        Boolean isSuccess = false;

        @Override
        protected void onPreExecute()
        {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String r)
        {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(MainActivity.this, r, Toast.LENGTH_SHORT).show();
            if(isSuccess)
            {
                Toast.makeText(MainActivity.this , "Inicio de sesion exitoso" , Toast.LENGTH_LONG).show();
                //finish();
            }
        }
        @Override
        protected String doInBackground(String... params)
        {
            String usernam = username.getText().toString();
            String passwordd = password.getText().toString();
            if(usernam.trim().equals("")|| passwordd.trim().equals(""))
                z = "Por favor ingrese sus credenciales";
            else
            {
                try
                {
                    con = connectionclass(un, pass, db, ip);        // Connect to database
                    if (con == null)
                    {
                        z = "Revise su conexion a Internet";
                    }
                    else
                    {
                        // Change below query according to your own database.
                        String query = "select * from Paciente where DNI= '" + usernam.toString() + "' and CONTRASEÑA = '"+ passwordd.toString() +"'  ";
                        String query2 = "select * from Personal_de_salud where DNI_medico= '" + usernam.toString() + "' and CONTRASEÑA = '"+ passwordd.toString() +"'  ";
                        Statement stmt = con.createStatement();
                        Statement stmt2=con.createStatement();
                        ResultSet rs = stmt.executeQuery(query);
                        ResultSet rs2= stmt2.executeQuery(query2);
                        if(rs.next())
                        {
                            z = "Inicio de Sesion Exitoso";
                            isSuccess=true;
                            Sesion.usuarioLogeado= username.getText().toString();
                            Intent myIntent2 = new Intent(MainActivity.this, PatientView.class);
                            startActivity(myIntent2);


                        }
                        else
                        {
                            if(rs2.next())
                            {
                                z = "Inicio de Sesion Exitoso";
                                isSuccess=true;
                                Sesion.usuarioLogeado= username.getText().toString();
                                Intent myIntent2 = new Intent(MainActivity.this, DoctorView.class);
                                startActivity(myIntent2);
                            }
                            else {
                                z = "Alguno de los datos ingresados son incorrectos";
                                isSuccess = false;
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    z = ex.getMessage();
                }
            }
            return z;
        }
    }


    @SuppressLint("NewApi")
    public Connection connectionclass(String user, String password, String database, String server)
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection connection = null;
        String ConnectionURL = null;
        try
        {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            ConnectionURL = "jdbc:jtds:sqlserver://" + server +";databaseName=" +database+ ";user=" + user+ ";password=" + password + ";";
            connection = DriverManager.getConnection(ConnectionURL);
        }
        catch (SQLException se)
        {
            Log.e("error here 1 : ", se.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            Log.e("error here 2 : ", e.getMessage());
        }
        catch (Exception e)
        {
            Log.e("error here 3 : ", e.getMessage());
        }
        return connection;
    }
}
