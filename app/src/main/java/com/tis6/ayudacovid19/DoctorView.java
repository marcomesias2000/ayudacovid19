package com.tis6.ayudacovid19;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DoctorView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_view);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.layoutname);

    }

    public void onClick(View view) {
        Intent myIntent;
        myIntent=null;
        switch (view.getId()){
            case R.id.btnhomeLauncher:
                myIntent=new Intent(DoctorView.this,DoctorView.class);
                break;


            case R.id.btnuserLauncher:
                myIntent=new Intent(DoctorView.this,UserProfileDoctor.class);
                break;

            case R.id.btnGestionambulancias:
                myIntent=new Intent(DoctorView.this,ManagementSos.class);
                break;

            case R.id.btnnotificaciones:
                myIntent=new Intent(DoctorView.this,NotificationsDoctor.class);
                break;

            case R.id.btndailycheckmanagement:
                myIntent=new Intent(DoctorView.this,ManagementDailyCheckList.class);
                break;

        }
        startActivity(myIntent);

    }
}