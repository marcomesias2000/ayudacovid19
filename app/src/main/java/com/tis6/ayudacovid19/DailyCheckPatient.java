package com.tis6.ayudacovid19;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DailyCheckPatient extends AppCompatActivity {
    RadioGroup radioGroup1;
    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioGroup radioGroup2;
    RadioButton radioButton3;
    RadioButton radioButton4;
    RadioGroup radioGroup3;
    RadioButton radioButton5;
    RadioButton radioButton6;
    RadioGroup radioGroup4;
    RadioButton radioButton7;
    RadioButton radioButton8;
    RadioGroup radioGroup5;
    RadioButton radioButton9;
    RadioButton radioButton0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_check_patient);
        radioGroup1 = findViewById(R.id.radgroup1);
        radioGroup2 = findViewById(R.id.radgroup2);
        radioGroup3 = findViewById(R.id.radgroup3);
        radioGroup4 = findViewById(R.id.radgroup4);
        radioGroup5 = findViewById(R.id.radgroup5);
        // radioButton1 = findViewById(R.id.radBtnSi1);
        // radioButton2 = findViewById(R.id.radBtnNo1);
        // radioButton3 = findViewById(R.id.radBtnSi2);
        // radioButton4 = findViewById(R.id.radBtnNo2);
        // radioButton5 = findViewById(R.id.radBtnSi3);
        // radioButton6 = findViewById(R.id.radBtnNo3);
        // radioButton7 = findViewById(R.id.radBtnSi4);
        // radioButton8 = findViewById(R.id.radBtnNo4);
        // radioButton9 = findViewById(R.id.radBtnSi5);
        // radioButton0 = findViewById(R.id.radBtnNo5);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbardailycheckpatient);
    }
    public void onClick(View view) throws SQLException {
        Intent myIntent;
        myIntent = null;
        switch (view.getId()){
            case R.id.btnhomeLauncher:

                myIntent=new Intent(DailyCheckPatient.this,PatientView.class);
                break;

            case R.id.btnenviarcontroldiario:

                String ip = "sqlserverdb.chjctcdkykee.sa-east-1.rds.amazonaws.com";
                String db = "control_paciente_domicilio_covid_19";
                String un = "admin";
                String pass = "sqlserver";
                Connection con= connectionclass(un,pass,db,ip);
                Statement stmt = con.createStatement();
                int rpta1= radioGroup1.getCheckedRadioButtonId();
                radioButton1 = (RadioButton)findViewById(rpta1);
                String respue1 = radioButton1.getText().toString();
                int rpta2 = radioGroup2.getCheckedRadioButtonId();
                radioButton3 = (RadioButton)findViewById(rpta2);
                String respue2 = radioButton3.getText().toString();
                int rpta3 = radioGroup3.getCheckedRadioButtonId();
                radioButton5 = (RadioButton)findViewById(rpta3);
                String respue3 = radioButton5.getText().toString();
                int rpta4 = radioGroup4.getCheckedRadioButtonId();
                radioButton7 = (RadioButton)findViewById(rpta4);
                String respue4 = radioButton7.getText().toString();
                int rpta5 = radioGroup5.getCheckedRadioButtonId();
                radioButton9 = (RadioButton)findViewById(rpta2);
                String respue5 = radioButton9.getText().toString();
                double id = Math.floor(Math.random()*100000);
                String idk = String.valueOf(id);
                String dnimed = Sesion.dnimedico;
                String dniuser = Sesion.usuarioLogeado;
                String ubigeo = Sesion.Ubigeo;
                String query = "INSERT INTO Control_Diario (DNI_medico,ID_CONTROL,RPTA1,RPTA2,RPTA3,RPTA4,RPTA5,DNI,UBI_GEO) VALUES ('45093646','"+idk+"','"+respue1+"','"+respue2+"','"+respue3+"','"+respue4+"','"+respue5+"','"+dniuser+"','"+ubigeo+"');";
                stmt.executeUpdate(query);
                Toast.makeText(DailyCheckPatient.this , "Control Diario Enviado" , Toast.LENGTH_LONG).show();
                myIntent=new Intent(DailyCheckPatient.this,PatientView.class);
                break;


            case R.id.btnuserLauncher:
                myIntent=new Intent(DailyCheckPatient.this,UserProfile.class);
                break;

        }
        startActivity(myIntent);


    }
    public Connection connectionclass(String user, String password, String database, String server)
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection connection = null;
        String ConnectionURL = null;
        try
        {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            ConnectionURL = "jdbc:jtds:sqlserver://" + server +";databaseName=" +database+ ";user=" + user+ ";password=" + password + ";";
            connection = DriverManager.getConnection(ConnectionURL);
        }
        catch (SQLException se)
        {
            Log.e("error here 1 : ", se.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            Log.e("error here 2 : ", e.getMessage());
        }
        catch (Exception e)
        {
            Log.e("error here 3 : ", e.getMessage());
        }
        return connection;
    }
}