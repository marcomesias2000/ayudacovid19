package com.tis6.ayudacovid19;

public class Patient {

    private String namepatient, surnamepatient, dnipatient, ubicationpatient;
    public Patient(String nombre, String apellido, String dni) {

    }
    public Patient(String namepatient, String surnamepatient, String dnipatient, String ubicationpatient) {
        this.namepatient = namepatient;
        this.surnamepatient = surnamepatient;
        this.dnipatient = dnipatient;
        this.ubicationpatient = ubicationpatient;
    }

    public String getNamepatient() {
        return namepatient;
    }

    public void setNamepatient(String namepatient) {
        this.namepatient = namepatient;
    }

    public String getSurnamepatient() {
        return surnamepatient;
    }

    public void setSurnamepatient(String surnamepatient) {
        this.surnamepatient = surnamepatient;
    }

    public String getDnipatient() {
        return dnipatient;
    }

    public void setDnipatient(String dnipatient) {
        this.dnipatient = dnipatient;
    }

    public String getUbicationpatient() {
        return ubicationpatient;
    }

    public void setUbicationpatient(String ubicationpatient) {
        this.ubicationpatient = ubicationpatient;
    }
}
