package com.tis6.ayudacovid19;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ManagementDailyCheckList extends AppCompatActivity {
    private RecyclerView recyclerViewPatient;
    private RecyclerViewAdaptador adaptadorPatient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_management_daily_check_list);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbardailycheckdoctor);

        recyclerViewPatient=(RecyclerView)findViewById(R.id.recyPatientList);
        recyclerViewPatient.setLayoutManager(new LinearLayoutManager(this));
        adaptadorPatient= new RecyclerViewAdaptador(obtenerPacientesBD());
        recyclerViewPatient.setAdapter(adaptadorPatient);
    }
    public List<Patient> obtenerPacientes(){
       List<Patient> patients = new ArrayList<>();
       patients.add(new Patient("das","das","sad","dsa") );
       return patients;
    }


    public List<Patient> obtenerPacientesBD()
    {
        List<Patient> patients = new ArrayList<>();
        try{
            String ip = "sqlserverdb.chjctcdkykee.sa-east-1.rds.amazonaws.com";
            String db = "control_paciente_domicilio_covid_19";
            String un = "admin";
            String pass = "sqlserver";
            Connection con= connectionclass(un,pass,db,ip);
            Statement st= con.createStatement();
            String dnipac= Sesion.usuarioLogeado;
            ResultSet rs = st.executeQuery(" select* from Paciente where DNI_medico = '"+dnipac.toString()+"' ");

            while (rs.next())
            {

             patients.add(new Patient(rs.getString("NOMBRE"),rs.getString("APELLIDO"),rs.getString("DNI"),"Lima"));

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return patients;

    }

    public Connection connectionclass(String user, String password, String database, String server)
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection connection = null;
        String ConnectionURL = null;
        try
        {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            ConnectionURL = "jdbc:jtds:sqlserver://" + server +";databaseName=" +database+ ";user=" + user+ ";password=" + password + ";";
            connection = DriverManager.getConnection(ConnectionURL);
        }
        catch (SQLException se)
        {
            Log.e("error here 1 : ", se.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            Log.e("error here 2 : ", e.getMessage());
        }
        catch (Exception e)
        {
            Log.e("error here 3 : ", e.getMessage());
        }
        return connection;
    }

    public void onClick(View view) {
    }
}