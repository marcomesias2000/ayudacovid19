package com.tis6.ayudacovid19;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.w3c.dom.CDATASection;

public class PatientView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_view);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.layoutname);
    }
    public void onClick(View view) {
        Intent myIntent;
        myIntent = null;
        switch (view.getId()){
            case R.id.btnhomeLauncher:
                myIntent=new Intent(PatientView.this,PatientView.class);
                break;


            case R.id.btnuserLauncher:
                myIntent=new Intent(PatientView.this,UserProfile.class);
                break;

            case R.id.btnPreguntasFrecuentes:
                myIntent=new Intent(PatientView.this,FrequentQuestions.class);
                break;

            case R.id.btnControlDiario:
                myIntent=new Intent(PatientView.this,DailyCheckPatient.class);
                break;

            case R.id.btnHelpMe:
                myIntent=new Intent(PatientView.this,PatientHelp.class);
                break;

        }


        startActivity(myIntent);


        }

}


