package com.tis6.ayudacovid19;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PatientHelp extends AppCompatActivity {
    TextView name, apellido, dni, ubicacion, hora,coorde,nadas;
    Button ambu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_help);
        name =findViewById(R.id.namesos);
        apellido = findViewById(R.id.textapesos);
        dni =findViewById(R.id.textdnisos);
        coorde=findViewById(R.id.textubisos);
        nadas=findViewById(R.id.textcoorde);
        hora=findViewById(R.id.textfechasos);
        ambu=findViewById(R.id.btnsolicitarambulancia);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbarsospatient);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationStart();
        }
        try {
            EjecutarConsulta();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }
    public void onClick(View view) throws SQLException {
        Intent myIntent = null;
        switch (view.getId()){
            case R.id.btnsolicitarambulancia:
                String ip = "sqlserverdb.chjctcdkykee.sa-east-1.rds.amazonaws.com";
                String db = "control_paciente_domicilio_covid_19";
                String un = "admin";
                String pass = "sqlserver";
                Connection con= connectionclass(un,pass,db,ip);
                String user;
                user = Sesion.usuarioLogeado;
                int dniuser = Integer.parseInt(user);
                double id = Math.floor(Math.random()*100000);
                String idk = String.valueOf(id);
                String ik = "AYUDA"+idk;
                String sq= nadas.getText().toString();
                Time today= new Time(Time.getCurrentTimezone());
                today.setToNow();
                int dia = today.monthDay;
                int mes = today.month;
                int year = today.year;
                int hour = today.hour;
                int minuto = today.minute;
                int segundo = today.second;
                Double lati = Sesion.latitud;
                String latit = String.valueOf(lati);
                Double longi = Sesion.Longitud;
                String longit = String.valueOf(longi);
                String medasi = Sesion.dnimedico.toString();
                int dnimed = Integer.parseInt(medasi);
                String estadopedido = "En espera";
                String query = "INSERT INTO Ayuda_SOS (IDAYUDASOS,DNI,DNI_medico,LATITUD,LONGITUD,ESTADOPEDIDOAMBULANCIA) VALUES ('"+ik+"','"+dniuser+"','"+dnimed+"','"+latit+"','"+longit+"','En Espera');";
                Statement stmt = con.createStatement();
                stmt.executeUpdate(query);
                Toast.makeText(PatientHelp.this , "Solicitud enviada" , Toast.LENGTH_LONG).show();
                myIntent=new Intent(PatientHelp.this,PatientView.class);
                break;
            case R.id.btnhomeLauncher:

                myIntent=new Intent(PatientHelp.this,PatientView.class);
                break;


            case R.id.btnuserLauncher:
                myIntent=new Intent(PatientHelp.this,UserProfile.class);
                break;

        }
        startActivity(myIntent);

    }
    private void locationStart() {
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        PatientHelp.Localizacion Local = new Localizacion();
        Local.setUbicacion(this);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);
        coorde.setText("Localización agregada");
        coorde.setText("");
    }
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationStart();
                return;
            }
        }
    }
    public void setLocation(Location loc) {
        //Obtener la direccion de la calle a partir de la latitud y la longitud
        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        loc.getLatitude(), loc.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);
                    coorde.setText("Mi direccion es: "
                            + DirCalle.getAddressLine(0));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public class Localizacion implements LocationListener {
        PatientHelp ubicacion;
        public PatientHelp getUbicacion() {
            return ubicacion;
        }
        public void setUbicacion(PatientHelp ubicacion) {
            this.ubicacion = ubicacion;
        }
        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion
            loc.getLatitude();
            Sesion.latitud = loc.getLatitude();
            loc.getLongitude();
            Sesion.Longitud = loc.getLongitude();
            String Text = "Mi ubicacion actual es: " + "\n Lat = "
                    + loc.getLatitude() + "\n Long = " + loc.getLongitude();
            nadas.setText(Text);
            this.ubicacion.setLocation(loc);
        }
        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado
            coorde.setText("GPS Desactivado");
        }
        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado
            coorde.setText("GPS Activado");
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }
    }
    public void EjecutarConsulta() throws SQLException {
        String ip = "sqlserverdb.chjctcdkykee.sa-east-1.rds.amazonaws.com";
        String db = "control_paciente_domicilio_covid_19";
        String un = "admin";
        String pass = "sqlserver";
        Connection con= connectionclass(un,pass,db,ip);
        String user;
        user = Sesion.usuarioLogeado.toString();
        String query="SELECT * FROM Paciente WHERE DNI='"+user.toString()+"'";
        Statement stmt = con.createStatement();
        ResultSet rs= stmt.executeQuery(query);
        if (rs.next())
        {
            String n = rs.getString(2);
            String ape = rs.getString(10);
            String d = rs.getString(7);
            String iddom =rs.getString(8);
            String medasi = rs.getString(9);
            Sesion.dnimedico =medasi.toString();
            String famasi= rs.getString(6);
            name.setText("Nombre: "+n);
            apellido.setText("Apellido: "+ape);
            dni.setText("DNI: "+d);
            Time today= new Time(Time.getCurrentTimezone());
            today.setToNow();
            int dia = today.monthDay;
            int mes = today.month;
            int year = today.year;
            int hour = today.hour;
            int minuto = today.minute;
            int segundo = today.second;
            hora.setText(+hour+":"+minuto+":"+segundo+","+dia+"/"+mes+"/"+year);
        }
        else
        {
            name.setText("dsadasdasdas");
        }


    }
    public Connection connectionclass(String user, String password, String database, String server)
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection connection = null;
        String ConnectionURL = null;
        try
        {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            ConnectionURL = "jdbc:jtds:sqlserver://" + server +";databaseName=" +database+ ";user=" + user+ ";password=" + password + ";";
            connection = DriverManager.getConnection(ConnectionURL);
        }
        catch (SQLException se)
        {
            Log.e("error here 1 : ", se.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            Log.e("error here 2 : ", e.getMessage());
        }
        catch (Exception e)
        {
            Log.e("error here 3 : ", e.getMessage());
        }
        return connection;
    }
}