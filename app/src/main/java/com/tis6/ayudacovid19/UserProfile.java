package com.tis6.ayudacovid19;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.FontsContract;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.transform.Result;

public class UserProfile extends AppCompatActivity {
    TextView name, apellido, dni, direccion, medAssigned, famAssigned;
    String un,pass,db,ip;
    Connection con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.layoutname);

        name=findViewById(R.id.textName);
        apellido= findViewById(R.id.textLastName);
        dni=findViewById(R.id.textDni);
        direccion=findViewById(R.id.textDirection);
        medAssigned=findViewById(R.id.textDrAssigned);
        famAssigned=findViewById(R.id.textFamilyAssigned);
        try {
            EjecutarConsulta();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }
    public void onClick(View view) {
        Intent myIntent;
        myIntent = null;
        switch (view.getId()){
            case R.id.btnhomeLauncher:

                myIntent=new Intent(UserProfile.this,PatientView.class);
                break;


            case R.id.btnuserLauncher:
                myIntent=new Intent(UserProfile.this,UserProfile.class);
                break;

        }
        startActivity(myIntent);


    }



    public void EjecutarConsulta() throws SQLException {
        ip = "sqlserverdb.chjctcdkykee.sa-east-1.rds.amazonaws.com";
        db = "control_paciente_domicilio_covid_19";
        un = "admin";
        pass = "sqlserver";
        con= connectionclass(un,pass,db,ip);
        String user = Sesion.usuarioLogeado;
        String query="SELECT * FROM Paciente WHERE DNI='"+user.toString()+"'";
        Statement stmt = con.createStatement();
        ResultSet rs= stmt.executeQuery(query);
        if (rs.next())
        {
            String n = rs.getString(2);
            String ape = rs.getString(10);
            String ubigeo = rs.getString(4);
            Sesion.Ubigeo = ubigeo;
            String d = rs.getString(7);
            String iddom =rs.getString(8);
            String medasi = rs.getString(9);
            String famasi= rs.getString(6);
            name.setText("Nombre: "+n);
            apellido.setText("Apellido: "+ape);
            dni.setText("DNI: "+d);

            String query2="SELECT * FROM Domicilio WHERE ID_DOMICILIO = '" + iddom + "' ";


            ResultSet rs2= stmt.executeQuery(query2);
            if (rs2.next()){
                String ave = rs2.getString(3);
                String calle = rs2.getString(4);
                direccion.setText("Direccion: Avenida "+ave+", Calle "+calle);
                String query3="SELECT * FROM Personal_de_salud WHERE DNI_medico = '"+medasi+"'";
                ResultSet rs3=stmt.executeQuery(query3);
                if (rs3.next()){
                    String nommed =rs3.getString(3);
                    String apemed = rs3.getString(6);
                    medAssigned.setText("Medico Asignado: "+nommed+" "+apemed);
                    String query4="SELECT * FROM Cuidador WHERE DNI_CUIDADOR = '"+famasi+"'";
                    ResultSet rs4=stmt.executeQuery(query4);
                    if (rs4.next()){
                        String nomfam = rs4.getString(2);
                        String apefam =rs4.getString(3);
                        famAssigned.setText("Familiar Asignado: "+nomfam+" "+apefam);
                    }
                    else
                    {
                        name.setText("ADSDAsads");
                    }
                }
                else
                {
                    name.setText("dsadas");
                }


            }
            else{
                name.setText("DASDASDSA");
            }

        }
        else
        {
            name.setText("dsadasdasdas");
        }


    }
    public Connection connectionclass(String user, String password, String database, String server)
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection connection = null;
        String ConnectionURL = null;
        try
        {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            ConnectionURL = "jdbc:jtds:sqlserver://" + server +";databaseName=" +database+ ";user=" + user+ ";password=" + password + ";";
            connection = DriverManager.getConnection(ConnectionURL);
        }
        catch (SQLException se)
        {
            Log.e("error here 1 : ", se.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            Log.e("error here 2 : ", e.getMessage());
        }
        catch (Exception e)
        {
            Log.e("error here 3 : ", e.getMessage());
        }
        return connection;
    }
}